var gulp = require('gulp');
var zip = require('gulp-zip');
var forceDeploy = require('gulp-jsforce-deploy');
var fs = require('fs');
var jsforce = require('jsforce');

var USER_NAME = "",
var USER_PASSWORD = "";
var LOGIN_URL = "https://login.salesforce.com";
var API_VERSION = "37.0";

//Global installation npm install jsforce-metadata-tools -g
//jsforce-retrieve -u USERNAME -p PASS+TOKEN -D ./dest_src

gulp.task('watch', function() {
    gulp.watch(['./src/package.xml','./src/classes/*.*'], function(event){
        console.log(event);
        gulp.start('deploy');
    });
});

gulp.task('zip-slds',function(){
    gulp.src('./bower_components/salesforce-lightning-design-system/assets/**')
    .pipe(zip('slds.resource'))
    .pipe(gulp.dest('./src/staticresources'));
});

gulp.task('deploy',function() {
    return gulp.src(['./src/package.xml',
    './src/classes/*.*',
    './src/staticresources/*.*'
    ],{base: '.'})
    .pipe(zip('pkg.zip'))
    .pipe(forceDeploy({
        username: USER_NAME,
        password: USER_PASSWORD,
        loginUrl: LOGIN_URL,
        version: API_VERSION
    }));
});

// define the default task and add the watch task to it
gulp.task('default', ['deploy','watch']);